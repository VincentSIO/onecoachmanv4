// import Vue from 'nativescript-vue'
const Vue = require('nativescript-vue/dist/index');
import LoginPage from './components/LoginPage.vue'


import Today from './components/Today';
import Balance from './components/Balance';
import Calendar from './components/Calendar';


import { TNSFontIcon, fonticon } from './nativescript-fonticon';
TNSFontIcon.debug = false;
TNSFontIcon.paths = {
    'fa': './fonts/font-awesome.css',
    'ion': './fonts/ionicons.css',
};
TNSFontIcon.loadCss();
Vue.filter('fonticon', fonticon);  

// import VueDevtools from 'nativescript-vue-devtools'

// if(TNS_ENV !== 'production') {
//   Vue.use(VueDevtools)
// }
// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = false;

Vue.registerElement('RadSideDrawer', () => require('nativescript-ui-sidedrawer').RadSideDrawer)


//vue principale 1er vue lancer APP ou LoginPage
new Vue({
  render: h => h('frame', [h(Calendar)])
}).$start()
